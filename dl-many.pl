#!/usr/bin/perl

use strict;
use warnings;

use LWP::Simple;
use Path::Tiny;
use Digest::MD5 qw(md5_hex);
use Image::Magick;
use Term::ANSIColor;

while (1) {
	getstore("https://astolfo.life/kinky", "tmp");
	
	my $f = path("tmp");
	
	my $sum = md5_hex($f->slurp);
	if (path($sum)->exists) {
		print colored("Already exists $sum\n", "red");
		next;
	}
	
	my $img = Image::Magick->new;
	my $readErr = $img->Read($f->canonpath);
	warn $readErr if $readErr;
	
	my ($width, $height) = $img->Get("width", "height");
	
	if ($width >= 1920 and $height >= 1080 and $width >= $height) {
		print colored("Keeping $sum: $width x $height\n", "green");
		$f->move($sum);
	} else {
		print colored("Yeeting $sum: $width x $height\n", "red");
		$f->remove;
	}
}

