/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const { join } = require('path')
const { readFileSync } = require('fs')
const metrics = require('./metrics')

class Http {
  redirect (res, path) {
    res.writeHead(302, { location: path })
    res.end()
  }

  json (res, data) {
    res.setHeader('content-type', 'application/json')
    res.end(JSON.stringify(data))
  }

  html (res, file) {
    res.setHeader('content-type', 'text/html')
    res.end(readFileSync(join(__dirname, '../../views', file), 'utf8').split('\n').slice(17).join('\n'))
  }

  wrapYeetBots (fn) {
    return (req, res, t) => {
      const bot = this._detectBot(req)
      if (bot) {
        metrics.increment(`bots.${bot}`)
        res.writeHead(404)
        return res.end()
      }
      return fn(req, res, t)
    }
  }

  _detectBot (req) {
    const reqUa = req.headers['user-agent']
    if (!reqUa) return null
    return Object.keys(Http.UserAgents).find(
      agents => Http.UserAgents[agents].find(ua => reqUa.includes(ua))
    )
  }
}

Http.UserAgents = {
  discord: [ 'https://discordapp.com' ],
  telegram: [ 'TelegramBot' ],
  twitter: [ 'TwitterBot' ],
  facebook: [ 'Facebot', 'facebookexternalhit/' ],
  keybase: [ 'Keybase' ],
  skype: [ 'SkypeUriPreview' ],
  reddit: [ 'redditbot/' ]
}
module.exports = new Http()
