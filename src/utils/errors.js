/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const { join } = require('path')
const { createReadStream } = require('fs')
const metrics = require('./metrics')

const asset404 = join('/var/www/astolfo-life', 'not-found.png')
const asset405 = join('/var/www/astolfo-life', 'method-not-allowed.png')
const asset5xx = join('/var/www/astolfo-life', 'error.gif')

module.exports = {
  404: (req, res) => {
    res.setHeader('content-type', 'image/png')
    res.writeHead(404)
    createReadStream(asset404).pipe(res)
    metrics.increment('error.404')
  },
  405: (req, res) => {
    res.setHeader('content-type', 'image/png')
    res.writeHead(405)
    createReadStream(asset405).pipe(res)
    metrics.increment('error.405')
  },
  '5xx': (req, res, err = 500) => {
    res.setHeader('content-type', 'image/gif') // btw gif is pronounced /ɡɪf/
    res.writeHead(err)
    createReadStream(asset5xx).pipe(res)
    metrics.increment('error.5xx')
  }
}
