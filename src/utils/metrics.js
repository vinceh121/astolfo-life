const metrics = require("metrics");

const report = new metrics.Report();

const reporter = new metrics.GraphiteReporter(report, "weeb.services", "127.0.0.1", 2003);
reporter.start(5 * 60 * 1000);

module.exports = {
	increment: function (name) {
		let g = report.getMetric(name);
		if (g == undefined) {
            g = new metrics.Counter();
            report.addMetric(name, g);
		}
		g.inc();
		return g;
	},
};
