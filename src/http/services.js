/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const providers = require('../providers')
const { http } = require('../utils')

const provider = (type) => (req, res) => providers.provide(req, res, type)

const basicService = (type, hasKinky) => {
  const service = { '/': provider(type) }
  if (hasKinky) service['/kinky'] = provider(`${type}_NSFW`)
  return service
}

const about = (req, res) => http.redirect(res, 'https://gitlab.com/vinceh121/astolfo-life');

const services = {
  astolfo: {
    '/': provider('ASTOLFO'),
    '/kinky': provider('ASTOLFO_NSFW'),
    '/gitlab': about,
    '/about': about,
    '/license': (req, res) => http.redirect(res, 'https://gitlab.com/vinceh121/astolfo-life/-/blob/master/LICENSE'),
    '/canistealthis': (req, res) => http.redirect(res, 'https://gitlab.com/vinceh121/astolfo-life/-/blob/master/LICENSE')
  },
  boi: basicService('ASTOLFO', true),
  trap: basicService('TRAP', true),
  '9s': basicService('9S', true),
  yiff: basicService('YIFF', true)
}

const aliases = {
  www: 'astolfo',
  wishmeluck: 'random'
}

module.exports = {
  services, aliases
}
