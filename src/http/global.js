/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const Provider = require("../providers/provider");
const { http, errors } = require("../utils");

module.exports = {
	"/robots.txt": (req, res) => res.end("User-agent: *\nDisallow: /"),
	"/providers": (req, res) => {
		if (req.method !== "GET") {
			return errors["405"](req, res);
		}
		http.json(res, { available: Provider.available, nsfw: Provider.nsfw });
	},
};
