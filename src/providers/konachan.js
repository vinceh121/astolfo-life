/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const AbstractDanbooru = require('./AbstractDanbooru')

class Konachan extends AbstractDanbooru {
  constructor() {
    super([
      'ASTOLFO', 'ASTOLFO_NSFW',
      'TRAP', 'TRAP_NSFW', // why? cause it's the best
      '9S', '9S_NSFW'
    ])
  }

  get url() {
    return 'https://konachan.com/post.json?tags=#{tag}&limit=500'
  }

  provide(type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost('astolfo', [Konachan.SAFE])
      case 'ASTOLFO_NSFW':
        return this._getPost('astolfo', [Konachan.QUESTIONABLE, Konachan.EXPLICIT])
      case 'TRAP':
        return this._getPost('trap', [Konachan.SAFE])
      case 'TRAP_NSFW':
        return this._getPost('trap', [Konachan.QUESTIONABLE, Konachan.EXPLICIT])
      case '9S':
        return this._getPost('yorha_unit_no._9_type_s', [Konachan.SAFE])
      case '9S_NSFW':
        return this._getPost('yorha_unit_no._9_type_s', [Konachan.QUESTIONABLE, Konachan.EXPLICIT])
      default:
        return null
    }
  }
}

Konachan.SAFE = 's'
Konachan.QUESTIONABLE = 'q'
Konachan.EXPLICIT = 'e'

module.exports = Konachan
