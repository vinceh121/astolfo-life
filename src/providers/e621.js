const AbstractDanbooru = require("./AbstractDanbooru");

class E621 extends AbstractDanbooru {
	constructor() {
		super(["ASTOLFO", "ASTOLFO_NSFW", "YIFF"]);
	}

	get url() {
		return "https://e621.net/posts.json?tags=#{tag}";
	}

	getTags(post) {
		const tags = [];
		for (let cat in post.tags) {
			post.tags[cat].forEach((t) => tags.push(t));
		}
		return tags;
	}

	provide(type) {
		switch (type) {
			case "ASTOLFO":
				return this._getPost("rider_of_black", [E621.SAFE]);
			case "ASTOLFO_NSFW":
				return this._getPost("rider_of_black", [
					E621.QUESTIONABLE,
					E621.EXPLICIT,
				]);
			case "YIFF":
				return this._getPost("", [
					E621.QUESTIONABLE,
					E621.EXPLICIT,
				]);
			default:
				return null;
		}
	}
}

E621.SAFE = "s";
E621.QUESTIONABLE = "q";
E621.EXPLICIT = "e";

module.exports = E621;
