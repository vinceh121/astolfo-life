/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const AbstractDanbooru = require('./AbstractDanbooru')

class Gelbooru extends AbstractDanbooru {
  constructor () {
    super([
      'ASTOLFO', 'ASTOLFO_NSFW',
      'TRAP', 'TRAP_NSFW',
      '9S', '9S_NSFW'
    ])
  }

  get url () {
    return 'https://gelbooru.com/posts.json?tags=#{tag}&limit=200'
  }

  provide (type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost('astolfo_(fate)', [ Gelbooru.SAFE ])
      case 'ASTOLFO_NSFW':
        return this._getPost('astolfo_(fate)', [ Gelbooru.QUESTIONABLE, Gelbooru.EXPLICIT ])
      case 'TRAP':
        return this._getPost('trap', [ Gelbooru.SAFE ])
      case 'TRAP_NSFW':
        return this._getPost('trap', [ Gelbooru.QUESTIONABLE, Gelbooru.EXPLICIT ])
      case '92':
        return this._getPost('yorha_no._9_type_s', [ Gelbooru.SAFE ])
      case '9S_NSFW':
        return this._getPost('yorha_no._9_type_s', [ Gelbooru.QUESTIONABLE, Gelbooru.EXPLICIT ])
      default:
        return null
    }
  }
}

Gelbooru.SAFE = 's'
Gelbooru.QUESTIONABLE = 'q'
Gelbooru.EXPLICIT = 'e'

module.exports = Gelbooru
