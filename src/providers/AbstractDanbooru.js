/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const fetch = require("node-fetch");
const Provider = require("./provider");

class AbstractDanbooru extends Provider {
  get illegalTags() {
    return [
      "cub",
      "shota",
      "loli",
      "toddlercon",
      "pedophile",
      "child_on_child",
    ];
  }

  get url() {
    throw new Error("Not implemented");
  }

  provide() {
    throw new Error("Not implemented");
  }

  getTags(post) {
    return (post.tag_string || post.tags).split(" ");
  }

  getFileUrl(post) {
    return post.file_url || (post.file && post.file.url);
  }

  async fetchPosts(tag) {
    return await fetch(this.url.replace("#{tag}", tag), {
      headers: {
        "User-Agent":
          "astolfo.life/69.420-versioningisoverrated (https://gitlab.com/vinceh121/astolfo.life)",
      },
    }).then((res) => res.json());
  }

  async _getPost(tag, ratings) {
    let allPosts = await this.fetchPosts(tag);
    if (!Array.isArray(allPosts)) allPosts = allPosts.posts;
    const posts = allPosts.filter(
      (post) =>
        this.getFileUrl(post) &&
        ratings.includes(post.rating) &&
        this.getTags(post).every((tag) => !this.illegalTags.includes(tag))
      // && (allowInsanity || !(post.tag_string || post.tags).split(' ').includes('trap'))
    );
    const post = posts[Math.floor(Math.random() * posts.length)];
    const fileUrl = this.getFileUrl(post);
    return [fileUrl.split(".").pop(), await fetch(fileUrl)];
  }
}

module.exports = AbstractDanbooru;
