/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const AbstractDanbooru = require('./AbstractDanbooru')

class Yandere extends AbstractDanbooru {
  constructor() {
    super([
      'ASTOLFO', 'ASTOLFO_NSFW',
      'TRAP', 'TRAP_NSFW', // I hate humanity for this, there should be way more
      '9S', '9S_NSFW'
    ])
  }

  get url() {
    return 'https://yande.re/post.json?tags=#{tag}&limit=500'
  }

  provide(type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost('astolfo_(fate)', [Yandere.SAFE])
      case 'ASTOLFO_NSFW':
        return this._getPost('astolfo_(fate)', [Yandere.QUESTIONABLE, Yandere.EXPLICIT])
      case 'TRAP':
        return this._getPost('trap', [Yandere.SAFE])
      case 'TRAP_NSFW':
        return this._getPost('trap', [Yandere.QUESTIONABLE, Yandere.EXPLICIT])
      case '9S':
        return this._getPost('yorha_no._9_type_s', [Yandere.SAFE])
      case '9S_NSFW':
        return this._getPost('yorha_no._9_type_s', [Yandere.QUESTIONABLE, Yandere.EXPLICIT])
      default:
        return null
    }
  }
}

Yandere.SAFE = 's'
Yandere.QUESTIONABLE = 'q'
Yandere.EXPLICIT = 'e'

module.exports = Yandere
