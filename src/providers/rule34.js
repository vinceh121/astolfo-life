/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const fetch = require("node-fetch");
const AbstractDanbooru = require('./AbstractDanbooru')
const xml2js = require('xml2js');

class Rule34 extends AbstractDanbooru {
  constructor() {
    super([
      // 'ASTOLFO', // while there is safe content for those tags on r34, there isn't enough to get a listing with at least one
      // 'TRAP',
      // '9S',
      'ASTOLFO_NSFW',
      'TRAP_NSFW',
      '9S_NSFW',
      'YIFF'
    ])
  }

  get url() {
    return 'https://rule34.xxx/index.php?page=dapi&s=post&q=index&tags=#{tag}'
  }

  provide(type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost('astolfo_(fate)', [Rule34.SAFE])
      case 'ASTOLFO_NSFW':
        return this._getPost('astolfo_(fate)', [Rule34.QUESTIONABLE, Rule34.EXPLICIT])
      case 'TRAP':
        return this._getPost('trap', [Rule34.SAFE])
      case 'TRAP_NSFW':
        return this._getPost('trap', [Rule34.QUESTIONABLE, Rule34.EXPLICIT])
      case '9S':
        return this._getPost('yorha_9s', [Rule34.SAFE])
      case '9S_NSFW':
        return this._getPost('yorha_9s', [Rule34.QUESTIONABLE, Rule34.EXPLICIT])
      case 'YIFF':
        return this._getPost('furry', [Rule34.QUESTIONABLE, Rule34.EXPLICIT])
      default:
        return null
    }
  }

  async fetchPosts(tag) {
    let xml = await fetch(this.url.replace("#{tag}", tag), {
      headers: {
        "User-Agent":
          "astolfo.life/69.420-versioningisoverrated (https://gitlab.com/vinceh121/astolfo.life)",
      },
    }).then((res) => res.text()).then((txt) => xml2js.parseStringPromise(txt.replace(/<\?.*\?>/, "")))
    return xml.posts.post.filter(o => o.$).map(o => o.$)
  }
}

Rule34.SAFE = 's'
Rule34.QUESTIONABLE = 'q'
Rule34.EXPLICIT = 'e'

module.exports = Rule34
