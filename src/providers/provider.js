/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

class Provider {
  constructor (features) {
    this.features = features
  }

  canProvide (feature) {
    return this.features.includes(feature)
  }
}

Provider.available = [
  'ASTOLFO',
  'TRAP', // Yes it exists and its a *shame* there is so little
  '9S'
]
Provider.nsfw = [
  'ASTOLFO_NSFW',
  'TRAP_NSFW', // ...
  '9S_NSFW',
  'YIFF'
]

module.exports = Provider
