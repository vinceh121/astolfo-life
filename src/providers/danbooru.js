/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const AbstractDanbooru = require('./AbstractDanbooru')

class Danbooru extends AbstractDanbooru {
  constructor() {
    super([
      'ASTOLFO', 'ASTOLFO_NSFW',
      'TRAP', 'TRAP_NSFW',
      '9S', '9S_NSFW',
      'YIFF'
    ])
  }

  get url() {
    return 'https://danbooru.donmai.us/posts.json?tags=#{tag}&limit=200'
  }

  provide(type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost('astolfo_(fate)', [Danbooru.SAFE])
      case 'ASTOLFO_NSFW':
        return this._getPost('astolfo_(fate)', [Danbooru.QUESTIONABLE, Danbooru.EXPLICIT])
      case 'TRAP':
        return this._getPost('trap', [Danbooru.SAFE])
      case 'TRAP_NSFW':
        return this._getPost('trap', [Danbooru.QUESTIONABLE, Danbooru.EXPLICIT])
      case '9S':
        return this._getPost('yorha_no._9_type_s', [Danbooru.SAFE])
      case '9S_NSFW':
        return this._getPost('yorha_no._9_type_s', [Danbooru.QUESTIONABLE, Danbooru.EXPLICIT])
      case 'YIFF':
        return this._getPost('furry', [Danbooru.QUESTIONABLE, Danbooru.EXPLICIT])
      default:
        return null
    }
  }
}

Danbooru.SAFE = 's'
Danbooru.QUESTIONABLE = 'q'
Danbooru.EXPLICIT = 'e'

module.exports = Danbooru
