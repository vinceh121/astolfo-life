/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const fetch = require('node-fetch')
const Provider = require('./provider')

class Reddit extends Provider {
  constructor() {
    super([
      'ASTOLFO', 'ASTOLFO_NSFW',
      'TRAP', 'TRAP_NSFW', // Sadly... this was disabled for weeb.services
      '9S', '9S_NSFW',
      'YIFF'
    ])
  }

  provide(type) {
    switch (type) {
      case 'ASTOLFO':
        return this._getPost(['Astolfo', 'TempleOfAstolfo'], false)
      case 'ASTOLFO_NSFW':
        return this._getPost(['Astolfo', 'TempleOfAstolfo'], true)
      case 'TRAP':
        return this._getPost('CuteTraps', false)
      case 'TRAP_NSFW':
        return this._getPost(['traphentai', 'DeliciousTraps', 'CuteTraps', 'trapgifs', 'traps', 'trapsarentgay'], true)
      case 'YIFF':
        return this._getPost(['yiff', 'yiffgif', 'gfur', 'yiffbulge', 'yiffcomics'], true)
      case '9S':
        return this._getPost(['2Bx9S', '9Siscute'], false)
      case '9S_NSFW':
        return this._getPost(['2Bx9S', '9Siscute'], true)
      default:
        return null
    }
  }

  async _getPost(sub, acceptNsfw) {
    if (Array.isArray(sub)) {
      sub = sub[Math.floor(Math.random() * sub.length)]
    }
    const hot = await fetch(`https://www.reddit.com/r/${sub}/hot.json?limit=100`, {
      headers: {
        'User-Agent': 'NodeJS:astolfo.life:v0.0.1 (by /u/vinceh121) - https://astolfo.life/gitlab, contact@vinceh121.me - based on weeb.services by Bowser65'
      }
    }).then(res => res.json())
    const posts = hot.data.children.map(p => p.data).filter(post => (acceptNsfw ? post.over_18 : !post.over_18 ) && post.post_hint === 'image')
    const post = posts[Math.floor(Math.random() * posts.length)]
    return [
      post.url.split('.').pop(),
      await fetch(post.url)
    ]
  }
}

module.exports = Reddit
