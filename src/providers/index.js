/*
 * Copyright (c) 2020 Weeb Services
 * Licensed under the Open Software License version 3.0
 */

const Konachan = require('./konachan')
const Danbooru = require('./danbooru')
const Yandere = require('./yandere')
const Gelbooru = require('./gelbooru')
const E621 = require('./e621')
const Reddit = require('./reddit')
const Rule34 = require('./rule34')
const Provider = require('./provider')
const errors = require('../utils/errors')
const metrics = require('../utils/metrics')
const yeetbot = require('../utils/http')

class Providers {
  constructor() {
    this.providers = [
      new Konachan(),
      new Danbooru(),
      new Yandere(),
      new E621(),
      //new Gelbooru(),
      new Reddit(),
      new Rule34()
      // @todo: nekobot.xyz
      // @todo: gelbooru
      // @todo: rule34.xxx
    ]

    this.provide = yeetbot.wrapYeetBots(this._provide.bind(this))
  }

  async _provide(req, res, type) {
    try {
      const data = await this.provideStream(type)
      if (!data) return errors['404'](req, res)

      if (Provider.nsfw.includes(type)) {
        metrics.increment('providers.nsfw')
      } else {
        metrics.increment('providers.sfw')
      }
      metrics.increment(`provider.${type.replace('_', '.').toLowerCase()}`)
      res.setHeader('content-type', `image/${data[0]}`)
      data[1].body.pipe(res)
    } catch (e) {
      console.error('Unexpected error in provide', e)
      return errors['5xx'](req, res)
    }
  }

  provideStream(type) {
    const provider = this._getProvider(type)
    if (!provider) return null
    try {
      return provider.provide(type)
    } catch (e) {
      console.error('Error in provider ' + provider.constructor.name)
      throw e
    }
  }

  _getProvider(type) {
    const available = this.providers.filter(p => p.canProvide(type))
    return available[Math.floor(Math.random() * available.length)]
  }
}

module.exports = new Providers()
